package id.co.asyst.amala.tools.approval.invalid.name.check.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    private static Logger logger = LogManager.getLogger(Utils.class);

    public void validationReq(Exchange exchange) throws ParseException {
        logger.info("validation....");
        int valid = 0;
        logger.info("identity :" + exchange.getProperty("identity"));
        logger.info("activityinfo :" + exchange.getProperty("activityinfo"));

        if (exchange.getProperty("identity") != null && !exchange.getProperty("identity").equals("")) {
            logger.info("validation identity");
            if (exchange.getProperty("activityinfo") != null && !exchange.getProperty("activityinfo").equals("")) {
                logger.info("validation activityinfo");
                valid = 1;
            } else {
                valid = 0;
            }
        } else {
            valid = 0;
        }

        exchange.setProperty("reqvalid", valid);
        logger.info("reqvalid :" + exchange.getProperty("reqvalid"));
    }

    public void validationActivemq(Exchange exchange) throws ParseException {
        logger.info("validation....");

        int mq= 0;
        logger.info("identity :" + exchange.getProperty("identitymq"));

        if (exchange.getProperty("identitymq") != null && !exchange.getProperty("identitymq").equals("")) {
            logger.info("validation identity activemq");
            mq =1;
        } else {
            mq = 0;
        }

        exchange.setProperty("reqmqvalid", mq);
        logger.info("reqmqvalid :" + exchange.getProperty("reqmqvalid"));
    }

    public void datetime(Exchange exchange) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("date", sdf2.format(createddate));
        logger.info("method datetime");
    }

    public void data(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        List<Map<String, Object>> body = in.getBody(List.class);
        logger.info("body 2nd query : " + body);
        String activityinfo = (String) body.get(0).get("activityinfo");
        String activityid = (String) body.get(0).get("activityid");
        String cardnumber = (String) body.get(0).get("cardnumber");
        String memberid = (String) body.get(0).get("memberid");
        String bookingpersonalias = (String) body.get(0).get("bookingpersonalias");

        exchange.setProperty("activityinfo", activityinfo);
        exchange.setProperty("activityid", activityid);
        exchange.setProperty("cardnumber", cardnumber);
        exchange.setProperty("memberid", memberid);
        exchange.setProperty("bookingpersonalias", bookingpersonalias);

    }

    public void getActivityid(Exchange exchange) {
        logger.info("getActivityid ");
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Map<String, Object> body = in.getBody(Map.class);
        String activityid = (String) body.get("activityid");
        exchange.setProperty("activityidonsplit", activityid);
        logger.info("activityidonsplit : " + exchange.getProperty("activityid"));

    }

    public static String GenerateId(String prefix, Date date, int numdigit) {


        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");

        Random rand = new Random();

        String dateFormat = "";

        if (date != null)
            dateFormat = sdf.format(date);

        String[] arr = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

        String randomkey = "";
        for (int i = 0; i < numdigit; i++) {

            int random = rand.nextInt(36);
            randomkey += arr[random];
        }

        if (prefix== null){
            prefix="APPRV";
        }

        return prefix + dateFormat + randomkey;
    }

    public void setId (Exchange exchange){
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String id;
        Date d = new Date();
        id = GenerateId("APPRV", d, 12);
        exchange.setProperty("id", id);



    }
}
